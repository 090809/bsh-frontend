import API from '../../boot/API'

export const specialEventItemNames = {
  EVENT_ITEMS_LIST_REQUEST: 'EVENT_ITEMS_LIST_REQUEST',
  EVENT_ITEMS_LIST_ERROR: 'EVENT_ITEMS_LIST_ERROR',
  EVENT_ITEMS_LIST_SUCCESS: 'EVENT_ITEMS_LIST_SUCCESS',

  CREATE_EVENT_ITEM_REQUEST: 'CREATE_EVENT_ITEM_REQUEST',
  CREATE_EVENT_ITEM_ERROR: 'CREATE_EVENT_ITEM_ERROR',
  CREATE_EVENT_ITEM_SUCCESS: 'CREATE_EVENT_ITEM_SUCCESS',

  UPDATE_EVENT_ITEM_REQUEST: 'UPDATE_EVENT_ITEM_REQUEST',
  UPDATE_EVENT_ITEM_ERROR: 'UPDATE_EVENT_ITEM_ERROR',
  UPDATE_EVENT_ITEM_SUCCESS: 'UPDATE_EVENT_ITEM_SUCCESS'
}

export const specialEventItemState = {
  specialEventItems: []
}

export const specialEventItemGetters = {
  specialEventItems: state => eventId => state.specialEventItems.filter((item) => {
    return item.event_id === eventId
  }),
  specialEventItemLikesCount: state => eventItemId => state.specialEventItemLikes.filter((like) => {
    return like.event_item_id === eventItemId
  }).length
}

export const specialEventItemActions = {
  [specialEventItemNames.EVENT_ITEMS_LIST_REQUEST]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      commit(specialEventItemNames.EVENT_ITEMS_LIST_REQUEST)
      API.get('event_items?_embed=event_item_likes')
        .then(resp => {
          commit(specialEventItemNames.EVENT_ITEMS_LIST_SUCCESS, resp.data)
          resolve(resp.data)
        })
        .catch(resp => {
          commit(specialEventItemNames.EVENT_ITEMS_LIST_ERROR)
          reject(resp)
        })
    })
  },
  [specialEventItemNames.CREATE_EVENT_ITEM_REQUEST]: ({ commit, dispatch }, event) => {
    return new Promise((resolve, reject) => {
      commit(specialEventItemNames.CREATE_EVENT_ITEM_REQUEST)
      API.post('event_items', event)
        .then(resp => {
          commit(specialEventItemNames.CREATE_EVENT_ITEM_SUCCESS, resp.data)
          resolve(resp)
        })
        .catch(resp => {
          commit(specialEventItemNames.CREATE_EVENT_ITEM_ERROR)
          reject(resp)
        })
    })
  },
  [specialEventItemNames.UPDATE_EVENT_ITEM_REQUEST]: ({ commit, dispatch }, event) => {
    return new Promise((resolve, reject) => {
      commit(specialEventItemNames.CREATE_EVENT_ITEM_REQUEST)
      API.patch(`event_items/${event.id}`, event)
        .then(resp => {
          commit(specialEventItemNames.UPDATE_EVENT_ITEM_SUCCESS, resp.data)
          resolve(resp)
        })
        .catch(resp => {
          commit(specialEventItemNames.UPDATE_EVENT_ITEM_ERROR)
          reject(resp)
        })
    })
  }
}

export const specialEventItemMutations = {
  [specialEventItemNames.EVENT_ITEMS_LIST_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [specialEventItemNames.EVENT_ITEMS_LIST_SUCCESS]: (state, events) => {
    state.status = 'success'
    state.specialEventItems = events
  },
  [specialEventItemNames.EVENT_ITEMS_LIST_ERROR]: (state) => {
    state.status = 'error'
  },

  [specialEventItemNames.CREATE_EVENT_ITEM_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [specialEventItemNames.CREATE_EVENT_ITEM_SUCCESS]: (state, event) => {
    state.status = 'success'
    state.specialEventItems.push(event)
  },
  [specialEventItemNames.CREATE_EVENT_ITEM_ERROR]: (state) => {
    state.status = 'error'
  },

  [specialEventItemNames.UPDATE_EVENT_ITEM_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [specialEventItemNames.UPDATE_EVENT_ITEM_SUCCESS]: (state, specialEventItem) => {
    state.status = 'success'
    let index = state.specialEventItems.findIndex((elem) => elem.id === specialEventItem.id)
    this.set(state.events, index, specialEventItem)
  },
  [specialEventItemNames.UPDATE_EVENT_ITEM_ERROR]: (state) => {
    state.status = 'error'
  }
}
