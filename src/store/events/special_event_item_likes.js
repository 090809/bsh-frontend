import API from '../../boot/API'
import { userNames } from '../users'

export const specialEventItemLikeNames = {
  EVENT_ITEM_LIKES_LIST_REQUEST: 'EVENT_ITEM_LIKES_LIST_REQUEST',
  EVENT_ITEM_LIKES_LIST_ERROR: 'EVENT_ITEM_LIKES_LIST_ERROR',
  EVENT_ITEM_LIKES_LIST_SUCCESS: 'EVENT_ITEM_LIKES_LIST_SUCCESS',

  CREATE_EVENT_ITEM_LIKE_REQUEST: 'CREATE_EVENT_ITEM_LIKE_REQUEST',
  CREATE_EVENT_ITEM_LIKE_ERROR: 'CREATE_EVENT_ITEM_LIKE_ERROR',
  CREATE_EVENT_ITEM_LIKE_SUCCESS: 'CREATE_EVENT_ITEM_LIKE_SUCCESS',

  UPDATE_EVENT_ITEM_LIKE_REQUEST: 'UPDATE_EVENT_ITEM_LIKE_REQUEST',
  UPDATE_EVENT_ITEM_LIKE_ERROR: 'UPDATE_EVENT_ITEM_LIKE_ERROR',
  UPDATE_EVENT_ITEM_LIKE_SUCCESS: 'UPDATE_EVENT_ITEM_LIKE_SUCCESS'
}

export const specialEventItemLikeState = {
  specialEventItemLikes: []
}

export const specialEventItemLikeGetters = {
  specialEventItemLikes: state => itemId => state.specialEventItemLikes.filter((like) => {
    return like.event_item_id === itemId
  })
}

export const specialEventItemLikeActions = {
  [specialEventItemLikeNames.EVENT_ITEM_LIKES_LIST_REQUEST]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      commit(specialEventItemLikeNames.EVENT_ITEM_LIKES_LIST_REQUEST)
      API.get('event_item_likes?_expand=event_item')
        .then(resp => {
          commit(specialEventItemLikeNames.EVENT_ITEM_LIKES_LIST_SUCCESS, resp.data)
          resolve(resp.data)
        })
        .catch(resp => {
          commit(specialEventItemLikeNames.EVENT_ITEM_LIKES_LIST_ERROR)
          reject(resp)
        })
    })
  },
  [specialEventItemLikeNames.CREATE_EVENT_ITEM_LIKE_REQUEST]: ({ commit, dispatch }, like) => {
    return new Promise((resolve, reject) => {
      commit(specialEventItemLikeNames.CREATE_EVENT_ITEM_LIKE_REQUEST)
      API.post('event_item_likes', like)
        .then(resp => {
          commit(specialEventItemLikeNames.CREATE_EVENT_ITEM_LIKE_SUCCESS, resp.data)
          dispatch('users/' + userNames.MODIFY_COIN_REQUEST, {
            userId: like.event_item.user_id,
            amount: 1
          })
          resolve(resp)
        })
        .catch(resp => {
          commit(specialEventItemLikeNames.CREATE_EVENT_ITEM_LIKE_ERROR)
          reject(resp)
        })
    })
  }
}

export const specialEventItemLikeMutations = {
  [specialEventItemLikeNames.EVENT_ITEM_LIKES_LIST_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [specialEventItemLikeNames.EVENT_ITEM_LIKES_LIST_SUCCESS]: (state, likes) => {
    state.status = 'success'
    state.specialEventItemLikes = likes
  },
  [specialEventItemLikeNames.EVENT_ITEM_LIKES_LIST_ERROR]: (state) => {
    state.status = 'error'
  },

  [specialEventItemLikeNames.CREATE_EVENT_ITEM_LIKE_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [specialEventItemLikeNames.CREATE_EVENT_ITEM_LIKE_SUCCESS]: (state, like) => {
    state.status = 'success'
    state.specialEventItemLikes.push(like)
  },
  [specialEventItemLikeNames.CREATE_EVENT_ITEM_LIKE_ERROR]: (state) => {
    state.status = 'error'
  }
}
