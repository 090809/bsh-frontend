import moment from 'moment'

export const specialEventState = { }

export const specialEventGetters = {
  specialEventsAll: state => state.events.filter((event) => {
    return event.is_special === true
  }),
  specialEvents: (state, getters) => getters.specialEventsAll.filter((event) => {
    let now = moment()
    return now.isBetween(moment(event.from), moment(event.to))
  })
}

export const specialEventActions = { }

export const specialEventMutations = { }
