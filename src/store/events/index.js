import { eventActions, eventGetters, eventMutations, eventState } from './event'
import { specialEventGetters, specialEventState, specialEventActions, specialEventMutations } from './special_events'
import {
  specialEventItemActions,
  specialEventItemGetters,
  specialEventItemMutations,
  specialEventItemState
} from './special_event_items'
import {
  specialEventItemLikeActions,
  specialEventItemLikeGetters,
  specialEventItemLikeMutations,
  specialEventItemLikeState
} from './special_event_item_likes'
import { eventRateActions, eventRateGetters, eventRateMutations, eventRateState } from './event_rate'
import { messageActions, messageGetters, messageMutations, messageState } from './messages'

const state = {
  status: '',
  events: [],
  ...eventState,
  ...specialEventState,
  ...specialEventItemState,
  ...specialEventItemLikeState,
  ...eventRateState,
  ...messageState
}

const getters = {
  ...eventGetters,
  ...specialEventGetters,
  ...specialEventItemGetters,
  ...specialEventItemLikeGetters,
  ...eventRateGetters,
  ...messageGetters
}

const actions = {
  ...eventActions,
  ...specialEventActions,
  ...specialEventItemActions,
  ...specialEventItemLikeActions,
  ...eventRateActions,
  ...messageActions
}

const mutations = {
  ...eventMutations,
  ...specialEventMutations,
  ...specialEventItemMutations,
  ...specialEventItemLikeMutations,
  ...eventRateMutations,
  ...messageMutations
}

export default {
  namespaced: true,
  getters,
  mutations,
  actions,
  state
}
