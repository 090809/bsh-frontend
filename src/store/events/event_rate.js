import API from '../../boot/API'

export const eventRateNames = {
  EVENT_RATES_LIST_REQUEST: 'EVENT_RATES_LIST_REQUEST',
  EVENT_RATES_LIST_ERROR: 'EVENT_RATES_LIST_ERROR',
  EVENT_RATES_LIST_SUCCESS: 'EVENT_RATES_LIST_SUCCESS',

  CREATE_EVENT_RATE_REQUEST: 'CREATE_EVENT_RATE_REQUEST',
  CREATE_EVENT_RATE_ERROR: 'CREATE_EVENT_RATE_ERROR',
  CREATE_EVENT_RATE_SUCCESS: 'CREATE_EVENT_RATE_SUCCESS',

  UPDATE_EVENT_RATE_REQUEST: 'UPDATE_EVENT_RATE_REQUEST',
  UPDATE_EVENT_RATE_ERROR: 'UPDATE_EVENT_RATE_ERROR',
  UPDATE_EVENT_RATE_SUCCESS: 'UPDATE_EVENT_RATE_SUCCESS'
}

export const eventRateState = {
  eventRates: []
}

export const eventRateGetters = {
  eventRatesByUserId: state => userId => state.eventRates.filter((rate) => {
    return rate.user_id === userId
  }),
  eventRates: state => eventId => state.eventRates.filter((rate) => {
    return rate.event_id === eventId
  }),
  eventRatesAvg: (state, getters) => eventId => {
    let total = 0
    let eventRates = getters.eventRates(eventId)
    eventRates.forEach((rate) => {
      total += rate.mark
    })
    return total / eventRates.length
  }
}

export const eventRateActions = {
  [eventRateNames.EVENT_RATES_LIST_REQUEST]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      commit(eventRateNames.EVENT_RATES_LIST_REQUEST)
      API.get('event_rates?_expand=user&_expand=event')
        .then(resp => {
          commit(eventRateNames.EVENT_RATES_LIST_SUCCESS, resp.data)
          resolve(resp.data)
        })
        .catch(resp => {
          commit(eventRateNames.EVENT_RATES_LIST_ERROR)
          reject(resp)
        })
    })
  },
  [eventRateNames.CREATE_EVENT_RATE_REQUEST]: ({ commit, dispatch }, event) => {
    return new Promise((resolve, reject) => {
      commit(eventRateNames.CREATE_EVENT_RATE_REQUEST)
      API.post('event_rates', event)
        .then(resp => {
          commit(eventRateNames.CREATE_EVENT_RATE_SUCCESS, resp.data)
          resolve(resp)
        })
        .catch(resp => {
          commit(eventRateNames.CREATE_EVENT_RATE_ERROR)
          reject(resp)
        })
    })
  },
  [eventRateNames.UPDATE_EVENT_RATE_REQUEST]: ({ commit, dispatch }, event) => {
    return new Promise((resolve, reject) => {
      commit(eventRateNames.CREATE_EVENT_RATE_REQUEST)
      API.patch(`event_rates/${event.id}`, event)
        .then(resp => {
          commit(eventRateNames.UPDATE_EVENT_RATE_SUCCESS, resp.data)
          resolve(resp)
        })
        .catch(resp => {
          commit(eventRateNames.UPDATE_EVENT_RATE_ERROR)
          reject(resp)
        })
    })
  }

}

export const eventRateMutations = {
  [eventRateNames.EVENT_RATES_LIST_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [eventRateNames.EVENT_RATES_LIST_SUCCESS]: (state, events) => {
    state.status = 'success'
    state.eventRates = events
  },
  [eventRateNames.EVENT_RATES_LIST_ERROR]: (state) => {
    state.status = 'error'
  },

  [eventRateNames.CREATE_EVENT_RATE_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [eventRateNames.CREATE_EVENT_RATE_SUCCESS]: (state, event) => {
    state.status = 'success'
    state.eventRates.push(event)
  },
  [eventRateNames.CREATE_EVENT_RATE_ERROR]: (state) => {
    state.status = 'error'
  },

  [eventRateNames.UPDATE_EVENT_RATE_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [eventRateNames.UPDATE_EVENT_RATE_SUCCESS]: (state, eventRate) => {
    state.status = 'success'
    let index = state.eventRates.findIndex((elem) => elem.id === eventRate.id)
    this.set(state.eventRates, index, eventRate)
  },
  [eventRateNames.UPDATE_EVENT_RATE_ERROR]: (state) => {
    state.status = 'error'
  }
}
