import API from '../../boot/API'
import { userNames } from '../users'

export const shopNames = {
  SHOP_ITEMS_LIST_REQUEST: 'SHOP_ITEMS_LIST_REQUEST',
  SHOP_ITEMS_LIST_SUCCESS: 'SHOP_ITEMS_LIST_SUCCESS',
  SHOP_ITEMS_LIST_ERROR: 'SHOP_ITEMS_LIST_ERROR',

  SHOP_ITEMS_BUY_REQUEST: 'SHOP_ITEMS_BUY_REQUEST',
  SHOP_ITEMS_BUY_SUCCESS: 'SHOP_ITEMS_BUY_SUCCESS',
  SHOP_ITEMS_BUY_ERROR: 'SHOP_ITEMS_BUY_ERROR'
}

const state = {
  shopItems: []
}

const getters = {
  shopItems: state => state.shopItems
}

const actions = {
  [shopNames.SHOP_ITEMS_LIST_REQUEST]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      commit(shopNames.SHOP_ITEMS_LIST_REQUEST)
      API.get('items')
        .then(resp => {
          commit(shopNames.SHOP_ITEMS_LIST_SUCCESS, resp.data)
          resolve(resp.data)
        })
        .catch(resp => {
          commit(shopNames.SHOP_ITEMS_LIST_ERROR)
          reject(resp)
        })
    })
  },
  [shopNames.SHOP_ITEMS_BUY_REQUEST]: ({ commit, dispatch, rootGetters }, shopItemId) => {
    return new Promise((resolve, reject) => {
      let item = state.shopItems.find((item) => {
        return item.id === shopItemId
      })
      if (!item) {
        return reject('Item not founded!')
      }
      dispatch('users/' + userNames.MODIFY_COIN_REQUEST, { amount: -item.price }, { root: true })
        .then(() => {
          API.post('item_user', {
            item_id: item.id,
            user_id: rootGetters['users/profile'].id,
            done: false
          })
          return resolve()
        })
    })
  }
}

const mutations = {
  [shopNames.SHOP_ITEMS_LIST_REQUEST]: (state) => {
    state.status = 'loading'
  },
  [shopNames.SHOP_ITEMS_LIST_SUCCESS]: (state, shopItems) => {
    state.status = 'success'
    state.shopItems = shopItems
  },
  [shopNames.SHOP_ITEMS_LIST_ERROR]: (state) => {
    state.status = 'error'
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
