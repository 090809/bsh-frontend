
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      {
        path: 'events',
        component: () => import('pages/events/IndexEvents.vue'),
        children: [
          {
            name: 'events',
            path: '',
            component: () => import('pages/events/Events.vue')
          },
          {
            name: 'events.card',
            path: 'card/:id',
            component: () => import('pages/events/Card.vue')
          },
          {
            name: 'events.map',
            path: 'map',
            component: () => import('pages/events/Map.vue')
          }
        ]
      },
      {
        path: 'profile',
        component: () => import('pages/profile/Profile.vue'),
        children: [
          {
            name: 'profile',
            path: '/',
            component: () => import('pages/profile/User.vue')
          },
          {
            path: 'chats',
            component: () => import('pages/profile/ChatIndex.vue'),
            children: [
              {
                path: '',
                name: 'profile.chats',
                component: () => import('pages/profile/Chats.vue')
              },
              {
                name: 'profile.chat.id',
                path: './:id',
                component: () => import('pages/profile/Chat.vue')
              }
            ]
          },
          // {
          //   name: 'profile.events.organised.create',
          //   path: 'create',
          //   component: () => import('pages/profile/Create.vue')
          // },
          {
            name: 'profile.events',
            path: 'events',
            component: () => import('pages/profile/Events.vue'),
            children: [
              {
                name: 'profile.events.organised.create',
                path: 'create',
                component: () => import('pages/profile/Create.vue')
              },
              {
                name: 'profile.events.organise.members',
                path: 'members',
                props: true,
                component: () => import('pages/profile/Members.vue')
              },
              {
                name: 'profile.events.organised',
                path: 'organised',
                component: () => import('pages/profile/Organise.vue'),
                children: [
                  {
                    name: 'profile.organise.confirm',
                    path: 'confirm',
                    component: () => import('pages/profile/Confirm.vue')
                  }
                  // {
                  //   name: 'profile.organise.members',
                  //   path: 'members',
                  //   component: () => import('pages/profile/Members.vue')
                  // }
                ]
              },
              {
                name: 'profile.events.visited',
                path: 'visited',
                component: () => import('pages/profile/Visit.vue')
              },
              {
                name: 'profile.events.create',
                path: 'create',
                component: () => import('pages/profile/Create.vue')
              }
            ]
          }
        ]
      },
      {
        path: 'shop',
        component: () => import('pages/shop/Shop.vue'),
        children: [
          {
            name: 'shop',
            path: '/'
          },
          {
            name: 'shop.card',
            path: 'card/:id',
            component: () => import('pages/shop/Card.vue')
          }
        ]
      },
      {
        path: 'daily',
        component: () => import('pages/daily/IndexDaily.vue'),
        children: [
          {
            name: 'daily',
            path: '',
            component: () => import('pages/daily/Daily.vue')
          },
          {
            name: 'daily.task',
            path: 'dailyevent/:id',
            component: () => import('pages/daily/Event.vue')
          }
        ]
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
