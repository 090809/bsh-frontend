import axios from 'axios'

export default axios.create({
  baseURL: 'https://bsh.bonch.dev/api/',
  headers: {
    common: {
      Authorization: `Bearer a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3`
    }
  }
})
